#!/usr/bin/env bash

# This script cleans up groups older than $DELTA_SECONDS
# requirements: curl, jq, perl

# To use set:
#  private_token: required
#  QA_SANDBOX_GROUP: name of the group, defaults to gitlab-qa-sandbox-group
#  DELTA_SECONDS: defaults to 6 hours
#  GITLAB_HOST: defaults to staging.gitlab.com

set -e
group="${QA_SANDBOX_GROUP:-gitlab-qa-sandbox-group}"
host="${GITLAB_HOST:-staging.gitlab.com}"
private_token="$1"

if [[ -z $private_token ]]; then
    echo "You must pass an access token into this script"
fi

pages=$(curl -sI "https://$host/api/v4/groups/$group/subgroups?private_token=$private_token" | perl -ne 'm/X-Total-Pages: (\d+)/ && print $1')
dir=$(mktemp -d)
mkdir -p "$dir"
echo "$pages"
current_time=$(date -u +%s)
six_hours_ago=$((current_time - ${DELTA_SECONDS:-21600}))
echo "Fetching $pages pages"
for i in $(seq 1 "$pages"); do
    echo "writing page $dir/subgroups-$i"
    curl -s "https://$host/api/v4/groups/$group/subgroups?private_token=$private_token&page=$i" > "$dir/subgroups-$i"
done
for fname in "$dir"/subgroups-*; do
    for subgroup in $(jq -r '.[] |  "\(.name),\(.id)"' < "$fname"); do
        name=$(echo "$subgroup" | cut -d, -f1)
        id=$(echo "$subgroup" | cut -d, -f2)
        group_date=$(echo "$name" | perl -ne 'm/(\d{4})-(\d{2})-(\d{2})-(\d{2})-(\d{2})-(\d{2})/ && do { print "$1-$2-$3 $4:$5:$6  " }')
        if [[ -n $group_date ]]; then
            date_epoch=$(date -d "$group_date" +%s)
            if [[ $date_epoch -lt $six_hours_ago ]]; then
                echo "Remove: $name $id $group_date"
                curl -s -X DELETE "https://$host/api/v4/groups/$id?private_token=$private_token"
            else
                echo "Keeping: $name $id $group_date"
            fi
        fi
    done
done
rm -rf "$dir"
