.PHONY: build-docker-ci

# This doesn't matter for the gitlab registry
# when using a personal access token
REGISTRY_ACCESS_USER ?= gl-infra

build-docker-ci:
	docker build -t registry.ops.gitlab.net/gitlab-com/gl-infra/traffic-generator/ci-image -f Dockerfile-ci .

push-docker-ci: build-docker-ci
ifndef REGISTRY_ACCESS_TOKEN
    $(error REGISTRY_ACCESS_TOKEN is required to push to the gitlab registry)
endif
	@docker login -u ${REGISTRY_ACCESS_USER} -p ${REGISTRY_ACCESS_TOKEN} registry.ops.gitlab.net
	docker push registry.ops.gitlab.net/gitlab-com/gl-infra/traffic-generator/ci-image
