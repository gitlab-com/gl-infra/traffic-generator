#!/usr/bin/env ruby
require 'json'

json_input = $stdin
summary_labels = {}
test_results = nil
json_input.read.each_line do |line|
  begin
    test_results = JSON.parse(line)
  rescue JSON::ParserError
    next
  end
end

exit(1) unless test_results

### Populate top level test labels
summary_labels['version'] = test_results['version']
test_results['summary'].each do |k, v|
  v = v.round(2) if k == 'duration'
  summary_labels[k] = v
end
examples = test_results['examples']
rspec_results = []
examples.each do |example|
  next unless example.key?('description')

  rspec_result = {
    'description' => example['description'],
    'run_time' => example.fetch('run_time', 0),
    'status' => example.fetch('status', 'unknown')
  }
  rspec_results << rspec_result
end
rspec_results.each do |rspec_result|
  puts "traffic_gen_gitlab_qa_results{manual=\"#{test_results['manual']}\",environment=\"#{test_results['environment']}\",stage=\"#{test_results['stage']}\",version=\"#{summary_labels['version']}\",description=\"#{rspec_result['description']}\",status=\"#{rspec_result['status']}\"} #{rspec_result['run_time'].round(2)}"
end
puts "traffic_gen_gitlab_qa_duration{manual=\"#{test_results['manual']}\",environment=\"#{test_results['environment']}\",stage=\"#{test_results['stage']}\",version=\"#{summary_labels['version']}\"} #{summary_labels['duration'].round(2)}"
puts "traffic_gen_gitlab_qa_failures{manual=\"#{test_results['manual']}\",environment=\"#{test_results['environment']}\",stage=\"#{test_results['stage']}\",version=\"#{summary_labels['version']}\"} #{summary_labels['failure_count'].round(2)}"
